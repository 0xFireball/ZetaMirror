﻿using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZetaMirror.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public bool ForceClose { get; set; }
        private WFAppContext wfApp;

        public MainWindow()
        {
            InitializeComponent();
            wfApp = new WFAppContext(this);
            Task.Factory.StartNew(() =>
            {
                Application.Run(wfApp);
            });
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            if (!ForceClose)
            {
                e.Cancel = true;
                this.Hide();
            }
        }
    }
}