﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace ZetaMirror.Windows
{
    internal class WFAppContext : ApplicationContext
    {
        private readonly MainWindow _mainWindow;
        private NotifyIcon _icon;
        private Container _wfContainer;
        private ContextMenuStrip _iconContextMenuStrip;

        public WFAppContext(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;
            InitializeComponent();
            _icon.Visible = true;
        }

        private void InitializeComponent()
        {
            _wfContainer = new Container();
            _icon = new NotifyIcon(_wfContainer)
            {
                Icon = Properties.Resources.logo,
                Text = Properties.Resources.WindowTitle,
            };
            _icon.DoubleClick += IconOnDoubleClick;
            _iconContextMenuStrip = new ContextMenuStrip();
            _iconContextMenuStrip.SuspendLayout();
            var exitBtn = new ToolStripMenuItem("Exit");
            exitBtn.Click += ExitBtnOnClick;
            _iconContextMenuStrip.Items.Add(exitBtn);
            _iconContextMenuStrip.ResumeLayout(false);
            _icon.ContextMenuStrip = _iconContextMenuStrip;
        }

        private void ExitBtnOnClick(object sender, EventArgs eventArgs)
        {
            _mainWindow.ForceClose = true;
            _mainWindow.Close();
        }

        private void IconOnDoubleClick(object sender, EventArgs eventArgs)
        {
            _mainWindow.Show();
            _mainWindow.Focus();
        }
    }
}